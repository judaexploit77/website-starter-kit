package handlers

import (
	"bytes"
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"geeks-accelerator/oss/website-starter-kit/internal/mid"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web/webcontext"
	"geeks-accelerator/oss/website-starter-kit/internal/platform/web/weberror"
	"geeks-accelerator/oss/website-starter-kit/internal/webroute"

	"github.com/ikeikeikeike/go-sitemap-generator/v2/stm"
)

const (
	TmplLayoutBase          = "default.html"
	TmplContentErrorGeneric = "error-generic.html"
)

type AppContext struct {
	Log               *log.Logger
	Env               webcontext.Env
	StaticDir         string
	TemplateDir       string
	Renderer          web.Renderer
	WebRoute          webroute.WebRoute
	PreAppMiddleware  []web.Middleware
	PostAppMiddleware []web.Middleware
}

// API returns a handler for a set of routes.
func APP(shutdown chan os.Signal, appCtx *AppContext) http.Handler {

	// Include the pre middlewares first.
	middlewares := appCtx.PreAppMiddleware

	// Define app middlewares applied to all requests.
	middlewares = append(middlewares,
		mid.Logger(appCtx.Log),
		mid.Errors(appCtx.Log, appCtx.Renderer),
		mid.Metrics(),
		mid.Panics())

	// Append any global middlewares that should be included after the app middlewares.
	if len(appCtx.PostAppMiddleware) > 0 {
		middlewares = append(middlewares, appCtx.PostAppMiddleware...)
	}

	// Construct the web.App which holds all routes as well as common Middleware.
	app := web.NewApp(shutdown, appCtx.Log, appCtx.Env, middlewares...)

	sm, err := newSitemap(appCtx.StaticDir, appCtx.TemplateDir, appCtx.WebRoute.WebsiteUrl(""))
	if err != nil {
		appCtx.Log.Fatalf("main : Sitemap failed: %+v", err)
	}

	// Add other routes here...

	// Register root
	smRes, err := sm.generate()
	if err != nil {
		appCtx.Log.Fatalf("main : Sitemap failed: %+v", err)
	}
	r := Root{
		StaticDir: appCtx.StaticDir,
		Renderer:  appCtx.Renderer,
		WebRoute:  appCtx.WebRoute,
		Sitemap:   smRes,
	}
	app.Handle("GET", "/", r.Index)
	app.Handle("GET", "/robots.txt", r.RobotTxt)
	app.Handle("GET", "/sitemap.xml", r.SitemapXml)

	// Register health check endpoint. This route is not authenticated.
	check := Check{}
	app.Handle("GET", "/v1/health", check.Health)
	app.Handle("GET", "/ping", check.Ping)

	// Handle static files/pages. Render a custom 404 page when file not found.
	static := func(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
		err := web.StaticHandler(ctx, w, r, params, appCtx.StaticDir, "")
		if err != nil {
			if os.IsNotExist(err) {
				rmsg := fmt.Sprintf("%s %s not found", r.Method, r.RequestURI)
				err = weberror.NewErrorMessage(ctx, err, http.StatusNotFound, rmsg)
			} else {
				err = weberror.NewError(ctx, err, http.StatusInternalServerError)
			}

			return web.RenderError(ctx, w, r, err, appCtx.Renderer, TmplLayoutBase, TmplContentErrorGeneric, web.MIMETextHTMLCharsetUTF8)
		}

		return nil
	}

	// Static file server
	app.Handle("GET", "/*", static)

	return app
}

type sitemap struct {
	*stm.Sitemap
	templateDir string
	staticDir   string
	locs        []string
	files       []string
}

func newSitemap(staticDir, templateDir, siteUrl string) (*sitemap, error) {
	// Build a sitemap.
	sm := stm.NewSitemap(1)
	sm.SetVerbose(false)
	sm.SetDefaultHost(siteUrl)
	sm.Create()

	resp := sitemap{
		Sitemap:     sm,
		templateDir: templateDir,
		staticDir:   staticDir,
	}

	return &resp, nil
}

func (sm *sitemap) addRoute(loc string, path string) error {
	f, err := os.Stat(path)
	if os.IsNotExist(err) {
		path = filepath.Join(sm.templateDir, "content", path)

		f, err = os.Stat(path)
		if os.IsNotExist(err) {
			path = filepath.Join(sm.staticDir, path)
			f, err = os.Stat(path)
		}

		if err != nil {
			return err
		}
	}

	for _, has := range sm.locs {
		if has == loc {
			return nil
		}
	}

	for _, has := range sm.files {
		if has == path {
			return nil
		}
	}

	priority := 0.5
	changefreq := "monthly"
	mobile := true

	switch loc {
	case "", "/", "site-index.html":
		loc = "/"
		priority = 0.9
		changefreq = "weekly"
	}

	sm.Add(
		stm.URL{{"loc", loc},
			{"changefreq", changefreq},
			{"mobile", mobile},
			{"priority", priority},
			{"lastmod", f.ModTime().Format(time.RFC3339)}})

	sm.locs = append(sm.locs, loc)
	sm.files = append(sm.files, path)

	return nil
}

func (sm *sitemap) generate() (*stm.Sitemap, error) {

	// Add all the static files to the sitemap. This not recursive.
	files, err := ioutil.ReadDir(sm.staticDir)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".html") || strings.HasSuffix(f.Name(), ".htm") {
			p := filepath.Join(sm.staticDir, f.Name())

			dat, err := ioutil.ReadFile(p)
			if err != nil {
				return nil, errors.WithStack(err)
			}

			doc, err := goquery.NewDocumentFromReader(bytes.NewReader(dat))
			if err != nil {
				return nil, errors.WithStack(err)
			}

			var noIndex bool
			doc.Find("meta[name=robots]").Each(func(i int, s *goquery.Selection) {
				if av, _ := s.Attr("content"); strings.ToLower(av) == "noindex" {
					noIndex = true
				}
			})

			if !noIndex {
				sm.addRoute(f.Name(), p)
			}
		}
	}

	return sm.Sitemap, nil
}
